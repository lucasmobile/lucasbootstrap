$(function() {

    $('.carousel').carousel({
        interval: 1000,
        touch: true
    });

    $('.carousel-control-prev').click(function() {
        $('#carouselExampleControls').carousel('prev');
    })

    $('.carousel-control-next').click(function() {
        $('#carouselExampleControls').carousel('next');
    })

    $('.dropdown').click(function() {
        $('#cadastro').removeClass('active');
    })

    // $('.dropdown').hover(function() {
    //     $('.dropdown-menu').stop(true, true).toggle('show');
    // }, function() {
    //     $('.dropdown-menu').stop(true, true).toggle('hide');
    // })

    $('.navbar-toggler').click(function() {
        var verificaClass = $(this).hasClass('collapse');
        if (!verificaClass) {
            $('#changeUser').removeClass('show');
        }
    })

    var i = 0;

    $('#reactPunch').click(function() {
        i++;
        $('.punchs').text('Quantidade de socos no React: ' + i);
        return false;
    })


    $('.list-group').find('li').each(function() {
        var itemActive = $(this).attr('aria-current');
        var i = 1;

        if(itemActive) {
            i++
            var nomeItem = $(this).html();
            console.log(nomeItem + ' é um item ativo');
        }
        console.log('Itens ativos: ' + i);   
    })

    // setTimeout(function() {
    //     $('.modal').modal({
    //         backdrop: 'static',
    //         keyboard: false,
    //     });
    // }, 2000)

    $('.toast').toast({
        delay: 5000
    })

    $('input.startToast').focus(function() {
        message = $(this).attr('data-toast-msg');
        $('.toast').find('.toast-body').html(message);
        $('.toast').find('.horaAtual').html(dataAtual());
        $('.toast').toast('show');
    })

    $('button.startToast, a.startToast').click(function() {
        message = $(this).attr('data-toast-msg');
        $('.toast').find('.toast-body').html(message);
        $('.toast').toast('show');
    })

    $('form').submit(function() {
        message = $(this).attr('data-toast-msg');
        $('.toast').find('.toast-body').html(message);
        $('.toast').toast('show');
        return false;
    })

    dataAtual = function () {
        var data = new Date();
        var hora = data.getHours();
        var min = (data.getMinutes() < 10) ? '0' + data.getMinutes() : data.getMinutes();
        var sec = data.getSeconds();

        return hora+'h'+min+'m';
    }

})